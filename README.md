pyTodo
======

pyTodo is a simple todo list manager, python written and massively extensible. pyTodo is built upon Flask framework to help in API building.

Content
-------

* [Project tree & requirements](#Project-tree-&-requirements)
* [Requirements](#API-requirements)
* [Database](#Database)
* [Security & User management](#Security-&-User-management)
* [Improvements](#Improvements)


### Project tree & requirements

Command to generate tree :

```
tree -I build .
```

Output :

```
.
├── Makefile
├── client.py
├── db_create.py
├── documentation
│   ├── Makefile
│   └── source
│       ├── conf.py
│       └── index.rst
├── instance
│   └── flask.cfg
├── pytodo
│   ├── __init__.py
│   └── views.py
├── pytodo.db
├── requirements.txt
├── run.py
└── test.py
```

### API requirements

```
coverage==4.4.1
Flask==0.12.2
Flask-Security==3.0.0
Flask-SQLAlchemy==2.2
pep8==1.7.0
Sphinx==1.6.2
SQLAlchemy==1.1.11
unittest2==1.1.0
```


### Database

pyTodo use an sqlite database as there is no concurrent access to manage. pyTodo use Flask-SqlAlchemy as ORM to help in DB management.

### Security & User management

pyTodo does not provide any user management system as it is built for only one user. pyTodo does not provide neither any security mecanism.


### Improvements

 * provide a database adapter for :
    - mysql
    - postgresql
    - sqlite
 * multi user experience
 * teams
 * improve task definition
 * provide a simple webUI to access API
