""" file: client.py provides Client class """
from pytodo import db, Task
import uuid
import datetime
import sys
import json


class Client(Task):
    """ Client class provide a simple CRUD to manage tasks """

    def __init__(self, action=None, debug=True):
        self.default_action = "list"
        self.available_actions = ['list', 'get', 'add', 'update', 'remove']
        self.debug = debug
        if action is not None:
            self.action = action
        else:
            self.action = self.default_action

    def usage(self):
        output = "\nUSAGE:\n"
        output += "======\n"
        output += "python {} <action> [<ID>]\n".format(sys.argv[0])
        output += " - list              (default) list all tasks\n"
        output += " - get ID            get one task identified by ID\n"
        output += " - add               add a new task\n"
        output += " - update ID         update one task identified by ID\n"
        output += " - remove ID         remove one task identified by ID\n"
        if self.debug:
            print output
            return output
        else:
            return ""

    def set_action(self):
        """ set action from command line """
        try:
            self.action = sys.argv[1]
        except:
            self.usage()
            # self.action = self.default_action
        return self.action

    def get_action(self):
        return self.action

    def add(self):
        print "------------------------------------------------------------------------"
        print " Add a new task"
        print "------------------------------------------------------------------------"
        uid = uuid.uuid1()
        name = raw_input('Give a name to new task :\n')
        print "------------------------------------------------------------------------"
        short = raw_input('Give a short description :\n')
        print "------------------------------------------------------------------------"
        desc = raw_input('Give a full description :\n')
        print "------------------------------------------------------------------------"
        due_date = raw_input('Due date ? (YYYY-MM-DD) :\n')
        print "------------------------------------------------------------------------"
        year, month, day = due_date.split('-')
        dday = datetime.datetime(int(year), int(month), int(day), 0, 0, 0, 0)
        prio = raw_input('Priority : Low / Medium / High :\n')
        print "------------------------------------------------------------------------"
        status = 'Todo'
        try:
            new_task = Task(public_id=str(uid), name=name, short_description=short, long_description=desc, due_date=dday, priority=prio, status=status)
            db.session.add(new_task)
            db.session.commit()
            print "\033[32mData recorded!\033[0m"
        except Exception as e:
            print e
            db.session.rollback()

    def list(self):
        """ list all tasks """
        all_tasks = Task.query.all()
        print "{} task(s) found :".format(len(all_tasks))
        print "------------------------------------------------------------------------"
        for task in all_tasks:
            dday = str(task.due_date)
            print "| {} | {} | {} | {}".format(task.public_id, task.name, dday.split()[0], task.priority)
        print "------------------------------------------------------------------------"

    def get(self):
        try:
            one_task = Task.query.filter_by(public_id=sys.argv[2]).first()
        except IndexError:
            self.usage()
            sys.exit(1)
        output = "------------------------------------------------------------------------\n"
        output += "{}\n".format(one_task.name)
        output += "------------------------------------------------------------------------\n"
        output += "{}\n".format(one_task.short_description)
        output += "------------------------------------------------------------------------\n"
        output += "{}\n".format(one_task.long_description)
        output += "------------------------------------------------------------------------\n"
        output += "due date : {} | priority : {}\n".format(one_task.due_date, one_task.priority)
        output += "------------------------------------------------------------------------\n"
        print output

    def update(self):
        try:
            current_task = Task.query.filter_by(public_id=sys.argv[2]).first()
        except IndexError:
            self.usage()
            sys.exit(1)
        print "------------------------------------------------------------------------"
        print " Update a task"
        print "------------------------------------------------------------------------"
        name = raw_input('Give a name to new task : {} \n'.format(current_task.name))
        print "------------------------------------------------------------------------"
        short = raw_input('Give a short description : {} \n'.format(current_task.short_description))
        print "------------------------------------------------------------------------"
        desc = raw_input('Give a full description : {} \n'.format(current_task.long_description))
        print "------------------------------------------------------------------------"
        due_date = raw_input('Due date ? (YYYY-MM-DD) : {} \n'.format(current_task.due_date))
        print "------------------------------------------------------------------------"
        year, month, day = due_date.split('-')
        dday = datetime.datetime(int(year), int(month), int(day), 0, 0, 0, 0)
        prio = raw_input('Priority : Low / Medium / High : {} \n'.format(current_task.priority))
        print "------------------------------------------------------------------------"
        status = raw_input('Status : {} \n'.format(current_task.status))
        print "------------------------------------------------------------------------"

    def remove(self):
        try:
            one_task = Task.query.filter_by(public_id=sys.argv[2]).first()
            db.session.delete(one_task)
            db.session.commit()
            print "\033[32mData deleted!\033[0m"
        except:
            db.session.rollback()
            print "\033[31mData not deleted!\033[0m"

    def run(self):
        """ run all client methods """
        self.set_action()
        if self.action == 'list':
            self.list()
        if self.action == 'get':
            self.get()
        if self.action == 'add':
            self.add()
        if self.action == 'update':
            self.update()
        if self.action == 'remove':
            self.remove()


if __name__ == "__main__":
    client = Client()
    client.run()
