from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
import uuid
import datetime


app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('flask.cfg')

# from . import views
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'

db = SQLAlchemy(app)


class Task(db.Model):
    __tablename__ = 'task'
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(100), unique=True)
    name = db.Column(db.String(160))
    short_description = db.Column(db.String(200))
    long_description = db.Column(db.Text)
    due_date = db.Column(db.DateTime, nullable=True)
    priority = db.Column(db.Enum('High', 'Medium', 'Low'), default='Medium')
    status = db.Column(db.Enum('Todo', 'Doing', 'Done'), default='Todo')

    def __init__(self, name, short_description, long_description, due_date, priority, status, public_id=None):
        self.public_id = public_id
        self.name = name
        self.short_description = short_description
        self.long_description = long_description
        self.due_date = due_date
        self.priority = priority
        self.status = status

    def set_uid(self):
        uid = uuid.uuid1()
        self.public_id = str(uid)

    def __repr__(self):
        return '<Task %r>' % self.name


@app.route('/', methods=['GET'])
def help():
    help = {'/': {'methods': ['GET'],
                  'GET': 'This help message'
                 },
            '/task': {'methods': ['GET', 'POST'],
                      'GET': 'Get all tasks',
                      'POST': 'Add new task'
                     },
            '/task/<id>': {'methods': ['GET', 'PUT'],
                           'GET': 'Get task definition',
                           'PUT': 'Update task identified by `id`'
                          }
            }
    return jsonify({'help': help})


@app.route('/task', methods=['GET'])
def get_all():
    all_task = Task.query.all()
    output = list()
    for task in all_task:
        output.append({'public_id': task.public_id,
                       'name': task.name,
                       'due_date': task.due_date,
                       'priority': task.priority,
                       'status': task.status
                    })
    return jsonify({'tasks': output})


@app.route('/task/<string:taskid>', methods=['GET'])
def get_one(taskid):
    one_task = Task.query.filter_by(public_id=taskid).first()
    output = {'name': one_task.name,
              'short_description': one_task.short_description,
              'long_description': one_task.long_description,
              'due_date': one_task.due_date,
              'priority': one_task.priority,
              'status': one_task.status
            }
    return jsonify({'task': output})


@app.route('/task', methods=['POST'])
def add():
    req = request.get_json()
    year, month, day = req['due_date'].split('-')
    dday = datetime.datetime(int(year), int(month), int(day), 0, 0, 0, 0)
    one_task = Task(name=req['name'], short_description=req['short_description'], long_description=req['long_description'], due_date=dday, priority=req['priority'], status=req['status'])
    one_task.set_uid()
    db.session.add(one_task)
    db.session.commit()
    return jsonify(req)

@app.route('/task/<string:taskid>', methods=['DELETE'])
def delete_one(taskid):
    one_task = Task.query.filter_by(public_id=taskid).first()
    try:
        db.session.delete(one_task)
        db.session.commit()
        return jsonify({'message' : 'task {} has been deleted'.format(taskid)})
    except:
        db.session.rollback()
        return jsonify({'message' : 'task {} has not been deleted'.format(taskid)})
