from client import Client
import unittest
import sys
from random import shuffle


class TestClient(unittest.TestCase):
    """ TestClient class provides tests to cover Client methods """

    def setUp(self):
        self.available_actions = ['list', 'get', 'add', 'update', 'remove']

    def tearDown(self):
        pass

    def test_set_action(self):
        """ test if action passed to class init works """
        for action in self.available_actions:
            self.client = Client(action=action, debug=False)
            new_action = self.client.set_action()
            self.assertEqual(new_action, action)

    def test_debug_false(self):
        """ test if debug mode False for testing purpose works """
        shuffle(self.available_actions)
        action = self.available_actions[0]
        print "\033[32m ===>>> {}\033[0m".format(action)
        self.client = Client(action=action, debug=False)
        res = self.client.usage()
        self.assertEqual(res, "")

    def test_debug_true(self):
        """ test if debug mode True for testing purpose works """
        shuffle(self.available_actions)
        action = self.available_actions[0]
        print "\033[32m ===>>> {}\033[0m".format(action)
        self.client = Client(action=action, debug=True)
        res = self.client.usage()
        self.assertNotEqual(len(res), 0)

    def test_list(self):
        self.client = Client(action='list', debug=False)
        self.client.run()


if __name__ == "__main__":
    unittest.main(verbosity=2)
